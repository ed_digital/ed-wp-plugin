<? return function() { ?>
  <div class="wrapper">
    <div class="row">
      <div class="col-8">
        <h1><a href="/" title="Go back to the homepage"><span>Website</span></a></h1>
      </div>
      <div class="col-4">
        <nav>
          <div class="main-menu">
            <? wp_nav_menu(); ?>
          </div>
        </nav>
      </div>
    </div>
  </div>
<? } ?>